﻿using System;
using System.Threading;

namespace ResetEventDemo
{
    class ResetEventHandle<T> where T : EventWaitHandle
    {
        /// <summary>test
        /// 通知一个或多个正在等待的线程已发生事件，处理器类型。      “等待线程 事件通知 处理器”.
        /// ManualResetEvent 继承自 ,EventWaitHandle:WaitHandle ， 表示一个线程同步事件。 
        /// EventWaitHandle 主要操作方法： Set() , Reset()。
        /// WaitHandle 封装等待对共享资源的独占访问的操作系统特定的对象
        /// </summary>
        private T _resetEventHandler;

        public ResetEventHandle(T resetEventHandler)
        {
            // 初始化一个  “等待线程 事件通知 处理器” 。
            // 参数 initialState ，初始状态是否为 终止：
            //     true   终止  ，即目前已无事件发生，无需等待程暂，线程继续运行
            //     false  非终止，即目前有事件发生，需等待，线停运行
            // this._mre = new ManualResetEvent(true);
            this._resetEventHandler = resetEventHandler;
        }

        public string CurTypeName { get { return _resetEventHandler.GetType().Name; } }
        /// <summary>
        /// 将事件状态设置为终止状态， 即目前已无事件发生，无需等待，其他等待中的线程继续运行
        /// </summary>
        public void Set() { this._resetEventHandler.Set(); }

        /// <summary>
        /// 将事件状态设置为非终止状态，即目前有事件发生，需等待，其他线程暂停运行
        /// </summary>
        public void Reset() { this._resetEventHandler.Reset(); }

        public void Run()
        {
            string strThreadID = string.Empty;
            try
            {
                while (true)
                {
                    // 阻塞当前线程 ，直到 _resetEventHandler 收到新的事件信号（即 set or reset）
                    this._resetEventHandler.WaitOne();

                    strThreadID = Thread.CurrentThread.ManagedThreadId.ToString();
                    Common.ConsoleWriteLine("线程 (" + strThreadID + ") 正在运行.");

                    Thread.Sleep(5000);
                }
            }
            catch (Exception ex)
            {
                Common.ConsoleWriteLine("线程 (" + strThreadID + ") 异常！错误：" + ex.Message.ToString());
            }
        }


    }
}