﻿using System;
using System.Threading;

namespace ResetEventDemo
{   
    class Program
    {
        static void Main(string[] args)
        {
            Common.ConsoleWriteLine("----------------------", false);
            Common.ConsoleWriteLine(" S 暂停      R 继续   ", false);

            var resetEventHandler = new ResetEventHandle<ManualResetEvent>(new ManualResetEvent(true));
            Common.ConsoleWriteLine(" Type: " + resetEventHandler.CurTypeName, false);
            Common.ConsoleWriteLine(" MainThread: ");
            Common.ConsoleWriteLine("\r\n", false);

            Common.CreateThreads(resetEventHandler.Run, 3);

            while (true)
            {
                string input = Console.ReadLine();
                if (input.Trim().ToLower() == "s")
                {
                    Common.ConsoleWriteLine("线程 暂停 运行.");
                    resetEventHandler.Reset();
                }
                else if (input.Trim().ToLower() == "r")
                {
                    Common.ConsoleWriteLine("线程 继续 运行.");
                    resetEventHandler.Set();
                }
            }

        }
    }

    class Common
    {
        public static void CreateThreads(Action runFunc, int tCount = 2)
        {
            var i = 0;
            while (i < tCount)
            {
                var t = new Thread(new ThreadStart(runFunc));
                t.Start();
                i++;
            }
        }

        public static void ConsoleWriteLine(string msg, bool withThreadSign = true, bool withDateTimeSign = false)
        {
            Console.WriteLine(
                msg +
                (withThreadSign ? (" [ThreadId: " + Thread.CurrentThread.ManagedThreadId.ToString() + " ]") : "") +
                (withDateTimeSign ? ("[" + System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss fff") + "]") : "")
                );
        }
    }
}
